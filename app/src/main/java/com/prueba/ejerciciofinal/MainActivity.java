package com.prueba.ejerciciofinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.prueba.ejerciciofinal.adapter.ContactoAdapter;
import com.prueba.ejerciciofinal.model.Contacto;
import com.prueba.ejerciciofinal.services.ContactoService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView rv = findViewById(R.id.rvcontactos);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://run.mocky.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ContactoService contactoService = retrofit.create(ContactoService.class);

        contactoService.allContacto().enqueue(new Callback<List<Contacto>>() {
            @Override
            public void onResponse(Call<List<Contacto>> call, Response<List<Contacto>> response) {
                if(response.code() == 200){
                    ContactoAdapter adapter = new ContactoAdapter(response.body());
                    rv.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<List<Contacto>> call, Throwable t) {
                Log.i("MAIN_APP","No se conecto ");
            }
        });







    }

    private List<Contacto> ListContacto (){
    List<Contacto> it = new ArrayList<>();
    Contacto c1 = new Contacto();
    c1.setNombre("Laura");
    c1.setNumero("924097775");
    c1.setImagenUrl("https://iteragrow.com/wp-content/uploads/2018/04/buyer-persona-e1545248524290.jpg");
    it.add(c1);

    Contacto c2 = new Contacto();
    c2.setNombre("Juan");
    c2.setNumero("904234587");
    c2.setImagenUrl("https://estudiantes.ucontinental.edu.pe/wp-content/uploads/2020/09/Madurez-emocional-7.jpg");
    it.add(c2);

    return it;
    }

}