package com.prueba.ejerciciofinal.services;

import com.prueba.ejerciciofinal.model.Contacto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ContactoService {

    @GET("v3/c5c309e1-65ce-41db-aa95-11c0262ed35f")
    Call<List<Contacto>> allContacto();

    @POST("2c6b0ea1-fb38-4769-aaa4-9432e53eeb1d")
    Call<Contacto> crear(@Body Contacto contacto);
}
