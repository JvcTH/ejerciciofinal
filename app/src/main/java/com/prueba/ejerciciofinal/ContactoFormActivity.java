package com.prueba.ejerciciofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.prueba.ejerciciofinal.model.Contacto;
import com.prueba.ejerciciofinal.services.ContactoService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContactoFormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto_form);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://webhook.site/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ContactoService contactoService = retrofit.create(ContactoService.class);


        Button botonCrear = findViewById(R.id.contSubirCont);

        EditText txtnombre = findViewById(R.id.contNombre);
        EditText txtnumero = findViewById(R.id.contNumero);
        EditText txtimagen = findViewById(R.id.contImgUrl);



        botonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Contacto contacto = new Contacto();
                contacto.setNombre(txtnombre.getText().toString());
                contacto.setNumero(txtnumero.getText().toString());
                contacto.setImagenUrl(txtimagen.getText().toString());

                contactoService.crear(contacto).enqueue(new Callback<Contacto>() {
                    @Override
                    public void onResponse(Call<Contacto> call, Response<Contacto> response) {

                        if(response.code() == 200){
                            Log.i("MAIN_APP","Se conecto correctamente ");
                        }else {
                            Log.i("MAIN_APP","Sucedio otro error");
                        }

                    }

                    @Override
                    public void onFailure(Call<Contacto> call, Throwable t) {
                        Log.i("MAIN_APP","No se conecto ");
                    }
                });


            }
        });


    }


}