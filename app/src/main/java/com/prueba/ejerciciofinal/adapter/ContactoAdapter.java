package com.prueba.ejerciciofinal.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prueba.ejerciciofinal.R;
import com.prueba.ejerciciofinal.model.Contacto;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactoAdapter extends RecyclerView.Adapter<ContactoAdapter.ContactoViewHolder> {

    private List<Contacto> data;

    public ContactoAdapter (List<Contacto> data){
        this.data=data;

    }
    
    @NonNull
    @Override
    public ContactoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacto,parent,false);
        return new ContactoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactoViewHolder holder, int i) {
        Contacto contacto= data.get(i);
        TextView tvnombre = holder.itemView.findViewById(R.id.txtnombre);
        tvnombre.setText(contacto.getNombre());

        TextView tvnumero = holder.itemView.findViewById(R.id.txtnumero);
        tvnumero.setText(contacto.getNumero());

        ImageView imgContacto = holder.itemView.findViewById(R.id.imgavatar);

        Picasso.get()
                .load(contacto.getImagenUrl())
                .into(imgContacto);





    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ContactoViewHolder extends RecyclerView.ViewHolder {

        public ContactoViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
